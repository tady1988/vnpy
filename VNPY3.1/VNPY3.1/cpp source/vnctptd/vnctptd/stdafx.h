#pragma once
#undef UNICODE
#undef _UNICODE
#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             
#include <windows.h>
#include "CTP/ThostFtdcUserApiDataType.h"
#include "CTP/ThostFtdcUserApiStruct.h"
#include "CTP/ThostFtdcTraderApi.h"
#include <string>
#include <iostream>
#include <map>
#include <fstream>
using namespace std;

#define PYCTPTRADER_EXPORTS
#define TYPE_NUM 20  //品种数量应该于MD 订阅一致


#define MY_VNDefInvestorPosition         WM_USER + 100
#define MY_OnFrontConnected             WM_USER + 101
#define MY_OnFrontDisconnected          WM_USER + 102
#define MY_OnRspUserLogin               WM_USER + 103
#define MY_OnRspUserLogout              WM_USER + 104
#define MY_OnRspQryInvestorPosition     WM_USER + 105
#define MY_OnRspQryTradingAccount       WM_USER + 106
#define MY_OnRtnOrder                   WM_USER + 107
#define MY_OnRtnTrade                   WM_USER + 108
#define MY_OnRtnDepthMarketData         WM_USER + 109
#define MY_OnRspSubMarketData           WM_USER + 110
#define MY_OnRspUnSubMarketData         WM_USER + 111
#define MY_OnRspForQuote                WM_USER + 112
#define MY_OnRspAuthenticate            WM_USER + 113
#define MY_IsErrorRspInfo               WM_USER + 114
 

#include "stdafx.h"
#include <windows.h>
#include <process.h>
#include <unordered_map>

extern HANDLE hStartEvent_VNDefInvestorPosition;
extern HANDLE hStartEvent_OnFrontConnected;
extern HANDLE hStartEvent_OnFrontDisconnected;
extern HANDLE hStartEvent_OnRspUserLogin;
extern HANDLE hStartEvent_OnRspUserLogout;
extern HANDLE hStartEvent_OnRspQryInvestorPosition;
extern HANDLE hStartEvent_OnRspQryTradingAccount;
extern HANDLE hStartEvent_OnRtnOrder;
extern HANDLE hStartEvent_OnRtnTrade;

extern unsigned nThreadID_OnFrontConnected;
extern unsigned nThreadID_OnFrontDisconnected;
extern unsigned nThreadID_OnRspUserLogin;
extern unsigned nThreadID_OnRspUserLogout;
extern unsigned nThreadID_OnRspQryInvestorPosition;
extern unsigned nThreadID_OnRspQryTradingAccount;
extern unsigned nThreadID_OnRtnOrder;
extern unsigned nThreadID_OnRtnTrade;


 


struct VNDEFTradeAcount
{
	char BrokerID[11];
	char InvestorID[13];
	char TradingDay[9];
	// 静态权益
	double prebalance;
	//动态权益
	double current;
	//可用权益
	double available;
	//今日盈亏
	double rate;
	//仓位
	double positionrate;
	//可取资金
	double WithdrawQuota; 
	//手续费
	double Commission;
}; 





///投资者持仓
struct VNDefInvestorPositionField222
{
	///合约代码
	TThostFtdcInstrumentIDType	InstrumentID;
	///经纪公司代码
	TThostFtdcBrokerIDType	BrokerID;
	///投资者代码
	TThostFtdcInvestorIDType	InvestorID;
	///持仓多空方向
	TThostFtdcPosiDirectionType	PosiDirection;
	///持仓日期
	TThostFtdcPositionDateType	PositionDate;
	///今日持仓
	TThostFtdcVolumeType	Position;
	///多头冻结
	TThostFtdcVolumeType	LongFrozen;
	///空头冻结
	TThostFtdcVolumeType	ShortFrozen;
	///开仓冻结金额
	TThostFtdcMoneyType	LongFrozenAmount;
	///开仓冻结金额
	TThostFtdcMoneyType	ShortFrozenAmount;
	///开仓量
	TThostFtdcVolumeType	OpenVolume;
	///平仓量
	TThostFtdcVolumeType	CloseVolume;
	///持仓成本
	TThostFtdcMoneyType	PositionCost;
	///上次占用的保证金
	TThostFtdcMoneyType	PreMargin;
	///占用的保证金
	TThostFtdcMoneyType	UseMargin;
	///冻结的保证金
	TThostFtdcMoneyType	FrozenMargin;
	///冻结的资金
	TThostFtdcMoneyType	FrozenCash;
	///冻结的手续费
	TThostFtdcMoneyType	FrozenCommission;
	///资金差额


	///上次结算价
	TThostFtdcPriceType	PreSettlementPrice;
	///本次结算价
	TThostFtdcPriceType	SettlementPrice;
	///交易日
	TThostFtdcDateType	TradingDay;



	///交易所代码
	TThostFtdcExchangeIDType	ExchangeID;


};


 

///投资者持仓
struct PositionDataType
{
	///合约代码
	TThostFtdcInstrumentIDType	InstrumentID;
	///经纪公司代码
	TThostFtdcBrokerIDType	BrokerID;
	///投资者代码
	TThostFtdcInvestorIDType	InvestorID;
	///持仓多空方向
	TThostFtdcPosiDirectionType	PosiDirection;
	///投机套保标志
	TThostFtdcHedgeFlagType	HedgeFlag;
	///持仓日期
	TThostFtdcPositionDateType	PositionDate;
	///上日持仓
	TThostFtdcVolumeType	YdPosition;
	///今日持仓
	TThostFtdcVolumeType	Position;
	///多头冻结
	TThostFtdcVolumeType	LongFrozen;
	///空头冻结
	TThostFtdcVolumeType	ShortFrozen;
	///开仓冻结金额
	TThostFtdcMoneyType	LongFrozenAmount;
	///开仓冻结金额
	TThostFtdcMoneyType	ShortFrozenAmount;
	///开仓量
	TThostFtdcVolumeType	OpenVolume;
	///平仓量
	TThostFtdcVolumeType	CloseVolume;
	///开仓金额
	TThostFtdcMoneyType	OpenAmount;
	///平仓金额
	TThostFtdcMoneyType	CloseAmount;
	///持仓成本
	TThostFtdcMoneyType	PositionCost;
	///上次占用的保证金
	TThostFtdcMoneyType	PreMargin;
	///占用的保证金
	TThostFtdcMoneyType	UseMargin;
	///冻结的保证金
	TThostFtdcMoneyType	FrozenMargin;
	///冻结的资金
	TThostFtdcMoneyType	FrozenCash;
	///冻结的手续费
	TThostFtdcMoneyType	FrozenCommission;
	///资金差额
	TThostFtdcMoneyType	CashIn;
	///手续费
	TThostFtdcMoneyType	Commission;
	///平仓盈亏
	TThostFtdcMoneyType	CloseProfit;
	///持仓盈亏
	TThostFtdcMoneyType	PositionProfit;
	///上次结算价
	TThostFtdcPriceType	PreSettlementPrice;
	///本次结算价
	TThostFtdcPriceType	SettlementPrice;
	///交易日
	TThostFtdcDateType	TradingDay;
	///结算编号
	TThostFtdcSettlementIDType	SettlementID;
	///开仓成本
	TThostFtdcMoneyType	OpenCost;
	///交易所保证金
	TThostFtdcMoneyType	ExchangeMargin;
	///组合成交形成的持仓
	TThostFtdcVolumeType	CombPosition;
	///组合多头冻结
	TThostFtdcVolumeType	CombLongFrozen;
	///组合空头冻结
	TThostFtdcVolumeType	CombShortFrozen;
	///逐日盯市平仓盈亏
	TThostFtdcMoneyType	CloseProfitByDate;
	///逐笔对冲平仓盈亏
	TThostFtdcMoneyType	CloseProfitByTrade;
	///今日持仓
	TThostFtdcVolumeType	TodayPosition;
	///保证金率
	TThostFtdcRatioType	MarginRateByMoney;
	///保证金率(按手数)
	TThostFtdcRatioType	MarginRateByVolume;
	///执行冻结
	TThostFtdcVolumeType	StrikeFrozen;
	///执行冻结金额
	TThostFtdcMoneyType	StrikeFrozenAmount;
	///放弃执行冻结
	TThostFtdcVolumeType	AbandonFrozen;
	///交易所代码
	TThostFtdcExchangeIDType	ExchangeID;
	///执行冻结的昨仓
	TThostFtdcVolumeType	YdStrikeFrozen;
	///投资单元代码
	TThostFtdcInvestUnitIDType	InvestUnitID;
	///大商所持仓成本差值，只有大商所使用
	TThostFtdcMoneyType	PositionCostOffset;
	///tas持仓手数
	TThostFtdcVolumeType	TasPosition;
	///tas持仓成本
	TThostFtdcMoneyType	TasPositionCost;
};


#define MAX_INSUTRUMENT 6000

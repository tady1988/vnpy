# MA策略
import talib
import module_backtest
from vnctptdType661 import *
from PyQt5 import QtCore
# CTP行情库
from vnctpmd import *
import numpy as np
import globalType
import globalvar

# 策略参数定义 '名称，开始值，结束值，步长'
parlist = [['short', 3, 10, 1], ['long', 10, 30, 1]]


class MyStrategy(module_backtest.VirtualAccount, QtCore.QThread):
    def __init__(self, period, slippoint):
        super(MyStrategy, self).__init__(period, slippoint)
        self.close=[]

    def OnTick(self, marketdata, strategyname):
        arg = [5, 20, 0, 0, 0, 0]
        InstrumentID = str(marketdata.InstrumentID, encoding="utf-8")
        kline = VNKlineData()
        kline.InstrumentID = marketdata.InstrumentID
        # kline.TradingDay = globalvar.md.GetKline(InstrumentID, 1)[0].TradingDay
        kline.TradingDay = int(str(globalvar.md.GetKline(InstrumentID, 1)[0].TradingDay, encoding="utf-8")  )
        kline.open = globalvar.md.GetKline(InstrumentID, 1)[0].Open
        kline.high = globalvar.md.GetKline(InstrumentID, 1)[0].High
        kline.low = globalvar.md.GetKline(InstrumentID, 1)[0].Low
        kline.close = globalvar.md.GetKline(InstrumentID, 1)[0].Close
        kline.volume = globalvar.md.GetKline(InstrumentID, 1)[0].Volume
        kline.klinetime = int(globalvar.md.GetKline(InstrumentID, 1)[0].Minutes)
        kline.money = 0
        kline.open_interest = 0
        print(str(kline.TradingDay) + "," +str(kline.open))
        self.OnKline(kline, arg, strategyname)
        '''
        #（1）marketdata 是 实时Tick数据
        #（2）这是从CTP Tick由vnctpmd.dll 在本机生成的K线，可以直接按下标方式取得，速度快vnctptd.dll、vnctpmd.py、vnctptd.ini
        #（3）这是从服务器获得当日K线取法，禁止频繁调用  vnklineservice.dll、vnklineservice.py、vnklineservice.ini
        #（4）这是当前显示K线图数据暂时，数据来自于（2）和（3），策略计算不用这个
        globalvar.data_kline_M1[0][globalvar.OPEN]
        globalvar.data_kline_M1[0][globalvar.HIGH]
        globalvar.data_kline_M1[0][globalvar.LOW]
        globalvar.data_kline_M1[0][globalvar.CLOSE]
        globalvar.data_kline_M1[0][globalvar.KLINETIME]
        globalvar.data_kline_M1[0][globalvar.VOL]
        globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.Minutes
        str(globalvar.data_kline_M1[0][globalvar.INSTRUMENT], encoding="utf-8")
        str(globalvar.data_kline_M1[0][globalvar.TRADINGDAY], encoding="utf-8")
        '''

    def OnKline(self, mddata, arg, strategyname):
        if arg[0] <= 0 or arg[1] <= 0:
            return
        # TradingDay = klinedata.TradingDay.decode()
        # klinetime = klinedata.klinetime.decode()
        self.InstrumentID = mddata.InstrumentID.decode()
        # self.exchange=mddata.exchange.decode()
        self.close.append(float(mddata.close))
        try:
            float_close = [float(x) for x in self.close]
        except Exception as e:
            pass
        self.MA_A = talib.MA(np.array(float_close), arg[0])
        self.MA_B = talib.MA(np.array(float_close), arg[1])
        print('结果1:' + str(self.MA_A))
        if self.MA_A[len(self.MA_A) - 1] > self.MA_B[len(self.MA_B) - 1]:
            if self.sellvol + self.sellvol_history > 0:
                self.InsertOrder(self.InstrumentID, '', THOST_FTDC_D_Buy, THOST_FTDC_OF_Close, VN_OPT_LimitPrice,
                                 mddata.close + 1, 1)
            if self.buyvol + self.buyvol_history < 10:
                self.InsertOrder(self.InstrumentID, '', THOST_FTDC_D_Buy, THOST_FTDC_OF_Open, VN_OPT_LimitPrice,
                                 mddata.close + 1, 1)
        elif self.MA_A[len(self.MA_A) - 1] < self.MA_B[len(self.MA_B) - 1]:
            if self.buyvol + self.buyvol_history > 0:
                self.InsertOrder(self.InstrumentID, '', THOST_FTDC_D_Sell, THOST_FTDC_OF_Close, VN_OPT_LimitPrice,
                                 mddata.close - 1, 1)
            if self.sellvol + self.sellvol_history < 10:
                self.InsertOrder(self.InstrumentID, '', THOST_FTDC_D_Sell, THOST_FTDC_OF_Open, VN_OPT_LimitPrice,
                                 mddata.close - 1, 1)

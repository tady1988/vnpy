#策略：MA
from vnctptdType661 import *
import globalvar
import pandas as pd
global MA_tj
MA_tj=0
def OnTick(marketdata, strategyname):
    print('OnTick MA:'+str(marketdata.InstrumentID, encoding="utf-8"))
    global MA_tj
    MA_tj=MA_tj+1
    if MA_tj>=60:
        MA_tj=0
        stock_data = pd.DataFrame(
            columns=['Open', 'High', 'Low', 'Close', 'KlineTime', 'Volume', 'Minutes',
                     'InstrumentID', 'TradingDay'])


        if True:
            #从Python获得K线数据，支持M1,M3,M5,M10,M15,M30,M60,M120,D1周期
            for i in range(600):
                stock_data.loc[i] = [globalvar.data_kline_M1[i][globalvar.OPEN],
                                     globalvar.data_kline_M1[i][globalvar.HIGH],
                                     globalvar.data_kline_M1[i][globalvar.LOW],
                                     globalvar.data_kline_M1[i][globalvar.CLOSE],
                                     globalvar.data_kline_M1[i][globalvar.KLINETIME],
                                     globalvar.data_kline_M1[i][globalvar.VOL],
                                     globalvar.md.GetKline(marketdata.InstrumentID, i).contents.Minutes,
                                     str(globalvar.data_kline_M1[i][globalvar.INSTRUMENT],encoding="utf-8"),
                                     str(globalvar.data_kline_M1[i][globalvar.TRADINGDAY],encoding="utf-8")]
        else:
            #通过Ctypes提供的方法获得从 C++ DLL 保存在内存的 M1 K线数据，C++只负责实时生成M1周期，其他周期应该由Python合成，所以本方式只适合M1周期
            for i in range(600):
                stock_data.loc[i] = [globalvar.md.GetKline(marketdata.InstrumentID, i).contents.Open,
                                     globalvar.md.GetKline(marketdata.InstrumentID, i).contents.High,
                                     globalvar.md.GetKline(marketdata.InstrumentID, i).contents.Low,
                                     globalvar.md.GetKline(marketdata.InstrumentID, i).contents.Close,
                                     globalvar.md.GetKline(marketdata.InstrumentID, i).contents.KlineTime,
                                     globalvar.md.GetKline(marketdata.InstrumentID, i).contents.Volume,
                                     globalvar.md.GetKline(marketdata.InstrumentID, i).contents.Minutes,
                                     str(globalvar.md.GetKline(marketdata.InstrumentID,i).contents.InstrumentID,encoding="utf-8"),
                                     str(globalvar.md.GetKline(marketdata.InstrumentID,i).contents.TradingDay,encoding="utf-8")]






            #print('[' + str(i) + ']数据A:%s' % (stock_data.loc[i]))
            #print('[' + str(i) + ']数据B:%s' % (stock_data.iloc[i]))

        # 将数据按照交易日期从远到近排序
        # stock_data.sort('date', inplace=True)
        # ========== 计算移动平均线
        # 分别计算5日、20日、60日的移动平均线
        ma_list = [5, 20, 60]

        # 计算简单算术移动平均线MA - 注意：stock_data['close']为股票每天的收盘价
        for ma in ma_list:
            # stock_data['MA_' + str(ma)] = pd.rolling_mean(stock_data['Close'], ma)
            stock_data['MA_' + str(ma)] = pd.Series.rolling(stock_data['Close'], ma).mean()
        # 将数据按照交易日期从近到远排序
        # stock_data.sort_values('TradingDay', ascending=False, inplace=True)
        # ========== 将算好的数据输出到csv文件 - 注意：这里请填写输出文件在您电脑中的路径
        # stock_data.to_csv('sh600000_ma_ema.csv', index=False)
        # 第1行数据
        print('MA数据:%s' % stock_data.iloc[0])
        # 第1行开盘价
        print('MA数据:%s' % stock_data.iloc[0]['Open'])
        # 第2行数据
        print('MA数据:%s' % stock_data.iloc[1])
        # 第2行开盘价
        print('MA数据:%s' % stock_data.iloc[1]['Open'])
        if (float(stock_data.iloc[0]['MA_5']) > float(stock_data.iloc[0]['MA_20'])):
            print('MA买')
            #用限价单 VN_OPT_LimitPrice 方式报单 。通常用涨停价限价单模拟市价，因为市价指令不支持所有的交易所
            exchangeid = globalvar.dict_exchange[str(marketdata.InstrumentID, encoding="utf-8")].split(',')[0]
            result = globalvar.td.InsertOrder(str(marketdata.InstrumentID, encoding="utf-8"), exchangeid, '0', '1',VN_OPT_LimitPrice , marketdata.LastPrice+10, 1)
            result = result + globalvar.td.InsertOrder(str(marketdata.InstrumentID, encoding="utf-8"), exchangeid, '0', '0',VN_OPT_LimitPrice , marketdata.LastPrice+10, 1)
            if result ==0:
                print('报单成功')
            else:
                print('报单失败')
        else:
            print('MA卖')
            #用限价单 VN_OPT_LimitPrice 方式报单 。通常用涨停价限价单模拟市价，因为市价指令不支持所有的交易所
            exchangeid = globalvar.dict_exchange[str(marketdata.InstrumentID, encoding="utf-8")].split(',')[0]
            result = globalvar.td.InsertOrder(str(marketdata.InstrumentID, encoding="utf-8"), exchangeid, '1', '1',VN_OPT_LimitPrice , marketdata.LastPrice-10, 1)
            result = result + globalvar.td.InsertOrder(str(marketdata.InstrumentID, encoding="utf-8"), exchangeid,'1', '0',VN_OPT_LimitPrice , marketdata.LastPrice-10, 1)
            if result ==0:
                print('报单成功')
            else:
                print('报单失败')


    '''
    方式1（推荐）：
    #本方式数据，先从服务器获取数据，再根据本地TICK数据自动生成，
    #断网重连会从新从服务器获取K线数据，是否从服务器获取初始的数据是可以选择的，具体在VNTrader “主菜单->K线数据来源” 选择
    #本方式K线数据是存在Python的变量里，定义在 globalvar.py ，globalvar.py存储的变量是共享的全局变量，方式各个Python文件调用
    #只有 data_kline_M1  (M1周期数据)是从服务器获取或实时生成
    # 其他周期K线数据是通过Python的Pandas本地合成
    # 本地生成数据包括 data_kline_M3、data_kline_M5、data_kline_M10、data_kline_M15、data_kline_M30、data_kline_M60、data_kline_M120、data_kline_D1
    #字段分别为 ID,Data,Time,Open,Close,Low,High
    #调用方式如下
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.TRADINGDAY]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.KLINETIME]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.INSTRUMENT]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.OPEN]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.CLOSE]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.LOW]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.HIGH]))
    print("data_kline_M1: " + str(data_kline_M1[0][globalvar.VOL]))
    
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.TRADINGDAY]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.KLINETIME]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.INSTRUMENT]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.OPEN]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.CLOSE]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.LOW]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.HIGH]))
    print("data_kline_M1: " + str(data_kline_M1[1][globalvar.VOL]))
    方式2：
    #从开源DLL获得M1 K线数据
    print("M1 K线合约名称 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.InstrumentID, encoding="utf-8"))
    print("M1 K线结束时间 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.KlineTime))
    print("M1 K线 最高价 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.High))
    print("M1 K线 最低价 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.Low))
    print("M1 K线 收盘价 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.Close))
    print("M1 K线 成交量 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.Volume))
    print("M1 K线 交易日 ref(0): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 0).contents.TradingDay, encoding="utf-8"))

    print("M1 K线合约名称 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.InstrumentID, encoding="utf-8"))
    print("M1 K线结束时间 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.KlineTime))
    print("M1 K线 最高价 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.High))
    print("M1 K线 最低价 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.Low))
    print("M1 K线 收盘价 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.Close))
    print("M1 K线 成交量 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.Volume))
    print("M1 K线 交易日 ref(1): " + str(globalvar.md.GetKline(marketdata.InstrumentID, 1).contents.TradingDay, encoding="utf-8"))
    '''


